{
    'name': 'Net Price',
    'version': '10.0.0.1',
    'summary': 'Net price in invoice and sale order',
    'description': 'Put the net price in order of purchase and invoice Net price = price - discount',
    'author': 'Raul Ovalle, raul@xmarts.do',
    'website': 'www.xmarts.net',
    'depends': ['sale',"account"],
    'data': ['views/sale_order_view.xml',
             'views/account_invoice_view.xml',],
    'installable': True,
}