from odoo import models, fields, api
import odoo.addons.decimal_precision as dp

class NewModule(models.Model):
    _name = 'account.invoice.line'
    _inherit = 'account.invoice.line'

    net_price = fields.Float('Precio Neto',compute='_compute_net_price_amount', digits=dp.get_precision('Product Price'))

    @api.one
    @api.depends('discount','price_unit')
    def _compute_net_price_amount(self):
        self.net_price = self.price_unit * (1 - (self.discount or 0.0) / 100.0)

