from odoo import models, fields, api
import odoo.addons.decimal_precision as dp

class NewModule(models.Model):
    _name = 'sale.order.line'
    _inherit = 'sale.order.line'

    net_price = fields.Float('Precio Neto',compute='_compute_net_price_amount', digits=dp.get_precision('Product Price'))

    @api.one
    @api.depends('discount','price_unit')
    def _compute_net_price_amount(self):
        """
        @api.depends() should contain all fields that will be used in the calculations.
        """
        for line in self:
            line.update({
                'net_price': line.price_unit * (1 - (line.discount or 0.0) / 100.0)
            })